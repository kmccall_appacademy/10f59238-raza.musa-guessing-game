# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game
  random_num = rand(1..100)
  guess = 1
  number_guessed = false
  while number_guessed == false
    puts "Guess a number"
    answer = gets.chomp.to_i
    puts "#{answer}"
    if answer > random_num
      puts "Number too high!"
      guess += 1
      puts guess
    elsif answer == 0 || answer < random_num
      puts "Number too low!"
      guess += 1
      puts guess
    else
      puts "Congratulations! It took you #{guess} tries to get the right answer"
      puts answer
      puts guess
      number_guessed = true
    end
  end
end
